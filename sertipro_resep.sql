-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 20, 2021 at 08:40 AM
-- Server version: 10.3.31-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sertipro_resep`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`appointment_id`, `user_id`, `patient_id`, `date`, `time`) VALUES
(1, 1, 4, '2020-01-15', '12:30 AM'),
(2, 4, 7, '2020-05-11', '3:00 PM'),
(3, 8, 10, '2020-06-04', '1:30 AM'),
(4, 8, 10, '2020-06-05', '1:30 AM'),
(5, 1, 12, '2020-06-04', '11:00 AM'),
(6, 10, 9, '2020-06-02', '9:30 AM'),
(7, 10, 9, '2020-06-05', '9:30 AM'),
(8, 10, 9, '2020-06-08', '9:30 AM');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `invoice_title` varchar(100) NOT NULL,
  `invoice_amount` varchar(100) NOT NULL,
  `invoice_date` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `user_id`, `patient_id`, `payment_mode`, `payment_status`, `invoice_title`, `invoice_amount`, `invoice_date`) VALUES
(6, 1, 1, 'Cheque', 'Unpaid', '[\"Amoxan Sirup\",\"Vitamin\",\"Jasa Laboratorium\",\"Jasa Dokter\"]', '[\"50000\",\"10000\",\"200000\",\"200000\"]', '2020-04-17'),
(5, 1, 1, 'Cash', 'Paid', '[\"Obat Maag\",\"Jasa Dokter\"]', '[\"10000\",\"120000\"]', '2020-04-17'),
(9, 1, 2, 'Cheque', 'Paid', '[\"Jasa Dokter\",\"Obat Maag\",\"Obat Sakit Kepala\"]', '[\"600000\",\"100000\",\"200000\"]', '2020-04-21'),
(8, 1, 1, 'Cheque', 'Unpaid', '[\"Obat Maag\",\"Tes Lab\",\"Jasa Dokter\"]', '[\"100000\",\"200000\",\"600000\"]', '2020-04-21'),
(10, 4, 6, 'Cash', 'Paid', '[\"Jasa Dokter\",\"Obat\"]', '[\"100000\",\"200000\"]', '2020-05-07'),
(11, 1, 12, 'Cash', 'Paid', '[\"resep\"]', '[\"75000\"]', '2020-06-02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `p_name` varchar(50) NOT NULL,
  `age` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `add` varchar(50) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `b_group` varchar(10) NOT NULL,
  `b_pressure` varchar(50) NOT NULL,
  `pulse` varchar(50) NOT NULL,
  `respiration` varchar(50) NOT NULL,
  `allergy` varchar(50) NOT NULL,
  `diet` varchar(10) NOT NULL,
  `other` text DEFAULT NULL,
  `date` date NOT NULL,
  `type` text NOT NULL,
  `owner` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `user_id`, `p_name`, `age`, `gender`, `phone`, `add`, `height`, `weight`, `b_group`, `b_pressure`, `pulse`, `respiration`, `allergy`, `diet`, `other`, `date`) VALUES
(5, 1, 'Tono Tono', '41', 1, '622153123667', 'Jl. Lombok 52 Bandung  Pusat', '175', '60', 'AB+', '80', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '110', 'null', '2020-05-03'),
(4, 1, 'Tina Tini', '23', 2, '622153123667', 'Jl Braga 52  Bandung', '165', '51', 'A+', '110', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', 'Diet Prote', 'null', '2020-05-03'),
(6, 4, 'Mari Mariana', '21', 2, '622153123667', 'Jl  Bromo 102 Malang Pusat', '165', '54', 'O+', '80', '111', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '110', 'null', '2020-05-03'),
(7, 4, 'Harto Hartono', '41', 1, '622153123667', 'Jl. Arjuno Raya 53 Malang', '180', '71', 'O-', '80', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '120', '[{\"label\":\"Kolesterol\",\"value\":\"200\"},{\"label\":\"Asam Urat \",\"value\":\"300\"}]', '2020-05-03'),
(8, 1, 'sunardi sukowardi', '41', 1, '1234567', 'Jl Sumatra', '170', '70', 'A+', '80', '111', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '110', 'null', '2020-06-01'),
(9, 10, 'Sunsuko', '41', 1, '1234567', 'Jakarta Barat', '180', '75', 'A-', '80', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '110', 'null', '2020-06-02'),
(10, 8, 'sunardi', '23', 1, '12345678', 'Jl Sumatra 1', '175', '70', 'A+', '80', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '110', 'null', '2020-06-02'),
(11, 11, 'Testing', '41', 1, '12345678', 'testing', '189', '80', 'A+', '80', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '111', 'null', '2020-06-02'),
(12, 1, 'promen', '13', 1, '+62813', 'tangsel', '120', '25', 'AB+', '90', '90', '90', '90', '90', '{\"1\":{\"label\":\"baik\",\"value\":\"bagus\"}}', '2020-06-02'),
(13, 1, 'Testing', '41', 2, '62811922695', 'teasting', '178', '65', 'A+', '80', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', '111', 'null', '2020-06-02');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `prescription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `symptoms` varchar(100) NOT NULL,
  `diagnosis` varchar(100) NOT NULL,
  `medicine` varchar(100) NOT NULL,
  `m_note` varchar(100) NOT NULL,
  `test` varchar(100) NOT NULL,
  `t_note` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `m_note2` text DEFAULT NULL,
  `t_note2` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`prescription_id`, `user_id`, `patient_id`, `symptoms`, `diagnosis`, `medicine`, `m_note`, `test`, `t_note`, `date`, `m_note2`, `t_note2`) VALUES
(2, 1, 1, 'Batuk Pilek dan demam', 'flu biasa', '[\"Aricept 5mg\",\"Aricept 5mg\"]', '[\"s 1 dd ;  10 tablet\",\"s 1 dd ;  10 tablet\"]', '', '', '2020-04-13', NULL, NULL),
(3, 1, 1, 'Back pain', 'LBP', '', '', '[\"Terapi  Laser\",\"Terapi Elektrik\",\"Manual Terapi\"]', '[\"5 mper 10 menit selama5 hari\",\"Selama 10 hari 100 voly\",\"latihan puggung selama 10 hari\"]', '2020-04-13', NULL, NULL),
(4, 1, 1, 'Panas  dan batuk', 'Influensa Biasa Bukan Covid19 ini bro', '[\"Panadol \",\"OBH Nelco\",\"Amoksan 500mg\"]', '[\"Sirup 60 ml  Fls 1\",\"Sirup 100 ml , Fls 1\",\"Tablet  XV\"]', '', '', '2020-04-13', '[\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\",\"S3 dd , Minum Setelah Makan\"]', NULL),
(5, 1, 1, 'Pusing', 'Kurang Makan', '[\"Amoksan 500mg\",\"Aricept 5mg\",\"Panadol\",\"Enervon C\"]', '[\"Tablet  XV\",\"Tablet  XV\",\"Tablet  XV\",\"Tablet  XV\"]', '', '', '2020-04-14', '[\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(6, 1, 1, 'Gatal di Bagian tumit cukup lama , luka tidak sembuh-sembuh\r\ngula darah tinggi', 'resiko ulkus diabetes\r\nperawatan luka', '', '', '[\"Wound Dresssing\",\"Cuci Boorwater\",\"NMT wound\"]', '[\"2 kali sehari\",\"2 kali sehari\",\"2kali per minggu\"]', '2020-04-14', NULL, '[\"25 hari\",\"25 hari\",\"30 hari\"]'),
(7, 1, 2, 'Sakit Kepala', 'Datang Bulan', '[\"Amoksan 500mg\",\"Enervon C\",\"Vitamin E\"]', '[\"s 1 dd ;  10 tablet\",\"s 1 dd ;  10 tablet\",\"s 1 dd ;  10 tablet\"]', '', '', '2020-04-21', '[\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(9, 1, 5, 'Batuk-batuk', 'Influensa', '[\"Amoksan 500mg\",\"OBH Nelco\"]', '[\"Tablet  XV\",\"Botol 1\"]', '', '', '2020-05-07', '[\"S1  Tab 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(11, 4, 6, 'Batuk-batuk', 'Faringitis aku', '[\"Amoksan 500mg\",\"Enervon C\"]', '[\"15 Tablet\",\"30 Tablet\"]', '', '', '2020-05-11', '[\"S1  Tab 3 dd, Minum Setelah Makan\",\"S1  Tab 3 dd, Minum Setelah Makan\"]', NULL),
(13, 10, 9, 'tsting', 'testing', '[\"Aricept 5mg\",\"Enervon C\",\"Vitamin E\"]', '[\"30 Tablet\",\"Tablet  XV\",\"30 Tablet\"]', '', '', '2020-06-02', '[\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1  Tab 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(14, 8, 10, 'testing', 'testing', '[\"Enervon C\",\"OBH Nelco\",\"Amoksan 500mg\"]', '[\"30 Tablet\",\"Botol 1\",\"30 Tablet\"]', '', '', '2020-06-02', '[\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\",\"S1  Tab 3 dd, Minum Setelah Makan\"]', NULL),
(15, 11, 11, 'Testing', 'testing', '[\"Aricept 5mg\",\"Enervon C\"]', '[\"30 Tablet\",\"30 Tablet\"]', '', '', '2020-06-02', '[\"S1  Tab 3 dd, Minum Setelah Makan\",\"S1  Tab 3 dd, Minum Setelah Makan\"]', NULL),
(16, 1, 12, 'bapil', 'bapil', '[\"pamol\"]', '[\"12\"]', '', '', '2020-06-02', '[\"3x1\"]', NULL),
(17, 1, 13, 'testiing', 'testing', '[\"Aricept 5mg\",\"Enervon C\"]', '[\"30 Tablet\",\"Botol 1\"]', '', '', '2020-06-02', '[\"S1  Tab 3 dd, Minum Setelah Makan\",\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(18, 1, 5, 'sakit kepala', 'vertigo', '[\"Vitamin E\"]', '[\"30 Tablet\"]', '', '', '2021-05-19', '[\"S1\\/2 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(19, 1, 4, 'batuk', 'batuk', '[\"Aricept 5mg\"]', '[\"30 Tablet\"]', '', '', '2021-05-27', '[\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL),
(20, 1, 13, 'Testing', 'Testing', '[\"Aricept 5mg\"]', '[\"30 Tablet\"]', '', '', '2021-06-01', '[\"S1 flsh 3 dd, Minum Setelah Makan\"]', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `doctor_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `favicon` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `office` text NOT NULL,
  `sip` text NOT NULL,
  `specialist` text NOT NULL,
  `status` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `doctor_name`, `email`, `mobile`, `password`, `logo`, `favicon`, `title`, `office`, `sip`, `specialist`, `status`) VALUES
(1, 'promedis', 'Dr. Sunardi Sukowardi', 'sunardisukowardi@gmail.com', '62215000000', 'UHJtMDYwNzA4MTA=', 'logo11.png', 'ic_promedis1.png', 'Promedis Resep', '[{\"address\":\"Jl. Braga 52-55 Lt3 Ruang 11 Bandung Selatan  \",\"day\":\"Senin - Jumat\",\"hour\":\"09.00 - 15.00\"},{\"address\":\"Jl Cipaganti 5  Rancaengkek Bandung \",\"day\":\"Senin-Selasa\",\"hour\":\"18.00-20.00\"},{\"address\":\"Jl. Gatot Soebroto 5 Bandung Utara Jawa Barat \",\"day\":\" Rabu-Jumat\",\"hour\":\"19.00 - 20.00\"}]', 'SIP : 1010199100/IDI/JBR/2020', 'Dokter Hewan', 2),
(4, 'Afif', 'Afif Program', 'helo@promedis.id', '62215172112', 'UGFzc21sZzAxMDE=', 'KlinikAfiah.png', '', '', '', '', '', 1),
(7, 'Vetbiz', 'Drh. Sunardi Sukowardi', 'sunardisukowardi@hotmail.com', '0811922695', 'UGFzc21sZzAxMDE=', 'logo_vb150x42.png', '', '', '', '', '', 1),
(8, 'Sunardi', 'Sunardi Sukowardi', 'helo@diskon.com', '02153123667', 'UGFzc21sZzAxMDE=', 'KlinikAfiah1.png', '246x0w1.png', 'Klinik Afiah', '[{\"address\":\"Jl Braga 52 Bandung Selatan  Jawa Barat 432999\",\"day\":\"Senin - Jumat\",\"hour\":\"09.00-20.00\"},{\"address\":\"Jl Cipaganti 5  Rancaengkek Bandung \",\"day\":\"Senin-Selasa\",\"hour\":\"18.00-20.00\"},{\"address\":\"Jl. Gatot Soebroto Kav 5 No.11A Bandung \",\"day\":\" Senin-Selasa\",\"hour\":\"19.00 - 20.00\"}]', 'SIP : 1010199100/IDI/JBR/2020', 'Ahli Bedah', 1),
(9, 'Patricia', 'Patricia Husada', 'kss@psglobalmedia.co.id', '0811922695', 'UHRjNjkwNjEz', '', '', 'Drg', '{\"0\":{\"address\":\"Jl Braga 52 Bandung Selatan  Jawa Barat 432999\",\"day\":\"Senin - Jumat\",\"hour\":\"09.00-20.00\"},\"2\":{\"address\":\"Jl. Gatot Soebroto 5 Bandung \",\"day\":\"Kamis - Sabtu \",\"hour\":\" 15.00-17.00\"},\"3\":{\"address\":\"Jl  Sudirman 52 Bogor\",\"day\":\"Sabtu\",\"hour\":\"10.00 - 12.00\"}}', 'SIP : 1010199100/IDI/JBR/2020', 'Dokter Gigi', 0),
(10, 'Livia', 'Andoko Wijaya', 'helo@sertipro.co.id', '08112911211', 'UGFzc3RncjAxMDE=', 'Screenshot_1.jpg', 'e-konsep2.png', 'Clinic Andoko', '{\"0\":{\"address\":\"Jl Braga 52 Bandung Selatan  Jawa Barat 432999\",\"day\":\"Senin - Jumat\",\"hour\":\"09.00-20.00\"},\"2\":{\"address\":\"Jl. Gatot Soebroto 5 Bandung Utara Jawa Barat \",\"day\":\"Kamis - Sabtu \",\"hour\":\"19.00 - 20.00\"},\"3\":{\"address\":\"Jl  Sudirman 52 Bogor\",\"day\":\"Sabtu\",\"hour\":\"10.00 - 12.00\"}}', 'SIP : 1010199100/IDI/JBR/2020', 'Gynacologist', 1),
(11, 'sunsuko', 'Anthony Clinic', 'helo@klikdiskon.com', '123456', 'UGFzc2prdDAxMDE=', 'Screenshot_1.jpg', 'pharmacist.png', 'Dr Athony', '[{\"address\":\"Jl Braga 52 Bandung Selatan  Jawa Barat 432999\",\"day\":\"Senin - Jumat\",\"hour\":\"09.00-20.00\"},{\"address\":\"Jl Cipaganti 5  Rancaengkek Bandung Utara Jawa Barat \",\"day\":\"Senin-Selasa\",\"hour\":\"18.00-20.00\"},{\"address\":\"Jl. Gatot Soebroto 5 Bandung \",\"day\":\"Kamis - Sabtu \",\"hour\":\" 15.00-17.00\"}]', '1.112.191.1910', 'Bedah Jantung', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
  ADD PRIMARY KEY (`prescription_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
  MODIFY `prescription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
