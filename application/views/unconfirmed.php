<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php');
  $data = $this->user_mo->get_user();
?>     

<!--Morris Chart CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/morris/morris.css">

<?php include_once('includes/header_end.php'); ?>

<div class="wrapper">
  <div class="container">
    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <div class="page-title-box">
          <div class="btn-group pull-right">
            <ol class="breadcrumb hide-phone p-0 m-0">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $data[0]['title']; ?></a></li>
              <li class="breadcrumb-item active">Belum di Konfirmasi Admin</li>
            </ol>
          </div>
          <h4 class="page-title">Belum di Konfirmasi Admin</h4>
        </div>
      </div>
    </div>
    <!-- end page title end breadcrumb -->
  </div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- ==================
   PAGE CONTENT START
   ================== -->

<div class="page-content-wrapper">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12">
        <div class="card m-b-20">
          <div class="card-block">
            <h4 class="text-center">Akun Anda Belum di Konfirmasi Admin</h4>
          </div>
        </div>
      </div> <!-- end col -->
    </div> <!-- end row -->
  </div><!-- container -->
</div> <!-- Page content Wrapper -->
		
<?php include_once('includes/footer_start.php'); ?>

<!--Morris Chart-->
<script src="<?php echo base_url(); ?>/assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/raphael/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>/assets/pages/morris.init.js"></script>

<?php include_once('includes/footer_end.php'); ?>