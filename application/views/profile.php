<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php');
  $data = $this->user_mo->get_user();
  $office = [['address' => '', 'day' => '', 'hour' => '']];
  $office = (empty($info['office'])) ? $office : json_decode($info['office'], true) ;
?>

    <!-- form Uploads -->
    <link href="<?php echo base_url(); ?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
<?php include_once('includes/header_end.php'); ?>

    <div class="wrapper">
      <div class="container">
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <div class="page-title-box">
              <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $data[0]['title']; ?></a></li>
                  <li class="breadcrumb-item active">Profil</li>
                </ol>
              </div>
              <h4 class="page-title">Pengaturan Profil</h4>
            </div>
          </div>
        </div><!-- end page title end breadcrumb -->
      </div> <!-- end container -->
    </div><!-- end wrapper -->
    <!-- ==================
        PAGE CONTENT START
    ================== -->
    <div class="page-content-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <label><h6>Informasi Profil</h6></label>
                <form name="updateprofile" id="updateprofile" method="post" action="<?php echo base_url('user_operation/updateprofile'); ?>">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nama Klinik/Praktek</label>
                        <input type="text" class="form-control" name="title" value="<?php echo $info['title'] ?>" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nama Dokter| Fisioterapi | Perawat</label>
                        <input type="text" class="form-control" name="name" value="<?php echo $info['doctor_name'] ?>" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Alamat Email</label>
                        <input type="email" class="form-control" name="email" value="<?php echo $info['email'] ?>" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nomor Telepon Kantor</label>
                        <input type="number" class="form-control" name="phone" value="<?php echo $info['mobile'] ?>" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Perbarui Informasi Profil</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div> <!-- end col -->
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <label><h6>Informasi Profil</h6></label>
                <form name="updatepassword" id="updatepassword" method="post" action="<?php echo base_url('user_operation/updatepassword'); ?>">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Kata Sandi Saat Ini</label>
                        <input type="password" class="form-control" name="cpass" id="cpass" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Kata Sandi Baru</label>
                        <input type="password" class="form-control" name="npass" id="npass" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Masukkan Lagi Kata Sandi</label>
                        <input type="password" class="form-control" name="rpass" id="rpass" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <button type="button" id="changepass" class="btn btn-primary waves-effect waves-light">Perbarui Kata Sandi</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div> <!-- end col -->
        </div> <!-- end row -->
        <div class="row">
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <form name="updatelogo" id="updatelogo" method="post" action="<?php echo base_url('user_operation/updatelogo'); ?>" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-md-12">
                      <label><h6>Perbarui Logo Klinik/Praktek</h6></label>
                      <button type="submit" class="btn btn-info waves-effect waves-light pull-right">Perbarui</button>
                    </div>
                    <div class="col-md-12">
                      <input type="file" name="logo" class="dropify" data-default-file="<?php echo base_url(); ?>assets/images/<?php echo $data[0]['logo']; ?>"  />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div> <!-- end col -->
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <form name="updatelogo" id="updatelogo" method="post" action="<?php echo base_url('user_operation/updatelogo/favicon'); ?>" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-md-12">
                      <label><h6>Perbarui Favicon</h6></label>
                      <button type="submit" class="btn btn-info waves-effect waves-light pull-right">Perbarui</button>
                    </div>
                    <div class="col-md-12">
                      <input type="file" name="logo" class="dropify" data-default-file="<?php echo base_url(); ?>assets/images/<?php echo $data[0]['favicon']; ?>"  />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div> <!-- end col -->
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <label><h6>Informasi Profil</h6></label>
                <form name="updateoffice" id="updateoffice" method="post" action="<?php echo base_url('user_operation/updateoffice'); ?>">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Spesialisasi</label>
                        <input type="text" class="form-control" name="specialist" value="<?php echo $info['specialist'] ?>" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>SIP</label>
                        <input type="text" class="form-control" name="sip" value="<?php echo $info['sip'] ?>" required="">
                      </div>
                    </div>
                    <div class="col-md-12" id="office" rel="<?php echo count($office); ?>">
                    <?php foreach ($office as $key => $val){ ?>
                      <?php if ($key != 0){ ?>
                        <hr class="praktek<?php echo $key;?>">
                      <?php } ?>
                      <div class="row praktek<?php echo $key;?>">
                        <div class="col-md-10">
                          <div class="form-group">
                            <label>Alamat Praktek</label>
                            <input type="text" class="form-control office" name="office[<?php echo $key; ?>][address]" required="" value="<?php echo $val['address']; ?>">
                            <label>Hari Praktek</label>
                            <input type="text" class="form-control office" name="office[<?php echo $key; ?>][day]" required="" value="<?php echo $val['day']; ?>">
                            <label>Jam Praktek</label>
                            <input type="text" class="form-control office" name="office[<?php echo $key; ?>][hour]" required="" value="<?php echo $val['hour']; ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                        <?php if ($key != 0){ ?>
                          <br><button type="button" class="btn btn-danger waves-effect waves-light pull-right" onclick="remform('<?php echo $key; ?>');"><i class="fa fa-close"></i></button>
                        <?php } else { ?>
                          &nbsp;
                        <?php } ?>
                        </div>
                      </div>
                    <?php } ?>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Perbarui Informasi Profesi</button>
                        <button type="button" class="btn btn-info waves-effect waves-light pull-right" onclick="addform('office');">Tambah Informasi Praktek</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div> <!-- end col -->
        </div> <!-- end row -->

      </div> <!-- end container -->
    </div><!-- end page-content-wrapper -->
<?php include_once('includes/footer_start.php'); ?>

    <!-- file uploads js -->
<script src="<?=base_url(); ?>assets/plugins/fileuploads/js/dropify.min.js"></script>

<script type="text/javascript">

function addform(id){
  var rel = parseInt($('#' + id).attr('rel'));
  var html = '<hr class="praktek' + rel + '"><div class="row praktek' + rel + '"><div class="col-md-10"><div class="form-group"><label>Alamat Praktek</label><input type="text" class="form-control office" name="office[' + rel + '][address]" required=""><label>Hari Praktek</label><input type="text" class="form-control office" name="office[' + rel + '][day]" required=""><label>Jam Praktek</label><input type="text" class="form-control office" name="office[' + rel + '][hour]" required=""></div></div><div class="col-md-2"><br><button type="button" class="btn btn-danger waves-effect waves-light pull-right" onclick="remform(' + rel + ');"><i class="fa fa-close"></i></button></div></div>';
  $('#' + id).attr('rel', rel + 1);
  $('#' + id).append(html);
}

function remform(id){
  $('.praktek' + id).remove();
}

$(document).ready(function(){
  
  $('#changepass').click(function(){
    var cpass = $('#cpass').val();
    var npass = $('#npass').val();
    var rpass = $('#rpass').val();
    
    if(rpass == npass) {
      $.ajax({
        type:'POST',
        url:'../user_operation/updatepassword',
        dataType:'json',
        data:{cpass : cpass,
          npass : npass
          },
        success: function(data){
            if(data.status == 1) {
              alert('Kata Sandi Berhasil Diganti.');	
            } else {
              alert('Silahkan Periksa Kata Sandi Lama Anda!!');
              $('#cpass').focus();
            }
        },
        error:function(data)
        {
          alert('Oops! Ada yang salah !!!');
        }
      });
    } else {
      alert("Kata Sandi Tidak Cocok!!!");
      $('#rpass').focus();
    }
  });

  $('.dropify').dropify({
    messages: {
      'default': 'Drag dan drop dokumen di sini atau Klik. <br>lebar 1024px Tinggi 1280px',
      'replace': 'Drag dan drop atau Klik Untuk Ganti. <br>lebar 1024px Tinggi 1280px',
      'remove': 'Hapus',
      'error': 'Ooops, Sesuatu terjadi.'
    },
    error: {
      'fileSize': 'File Terlalu Besar (1M max).'
    }
  });
});
</script>

<?php include_once('includes/footer_end.php'); ?>