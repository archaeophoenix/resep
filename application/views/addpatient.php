<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php'); 
?>

<?php include_once('includes/header_end.php');
  $data = $this->user_mo->get_user();
  $error = $this->session->flashdata('error');

  $name = 'Masukkan Nama Lengkap Pasien (Hanya Boleh Huruf Saja)';
  $age = 'Masukkan Usia Pasien (Hanya Angka Max 3 Digit)';
  $type = 'Masukkan Jenis Hewan(Hanya Boleh Huruf Saja)';
  $owner = 'Masukkan Nama Lengkap Pemilik';
  $phone = '+6251256767 (Hanya Angka Min 5 Digit Max 20 Digit)';
  $height = (TYPE == 'vetbiz') ? 'Nomor Rekam Medis (Hanya Angka)' : 'Masukkan Tinggi Badan (Cm) (Hanya Angka)';
  $weight = 'Masukkan Berat Badan (Kg) (Hanya Angka)';
  $b_pressure = (TYPE == 'vetbiz') ? 'Masukkan ID Microchip (Hanya Angka)' : 'Masukkan Tekanan Darah Sistole dan Diastole (Hanya Angka)';
  $pulse = (TYPE == 'vetbiz') ? 'Masukkan Suhu Tubuh (C) (Hanya Angka)' : 'Masukkan Denyut Nadi (Kali/Menit) (Hanya Angka)';
  $respiration = (TYPE == 'vetbiz') ? 'Frekuensi Pernafasan Per Detik (Hanya Angka)' : 'Masukkan Tes Respirasi: FEV1; FVC; Rasio FVC/FEV1 Per Detik (Hanya Angka)';
  $allergy = (TYPE == 'vetbiz') ? 'Masukkan Anamnesa' : 'Masukkan Gejala Alergi & Penyakit Saat Ini';
  $diet = (TYPE == 'vetbiz') ? 'Hasil Pengamatan Tingkah Laku' : 'Masukkan Informasi Program Diet';
?>
  
  <div class="wrapper">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-title-box">
            <div class="btn-group pull-right">
              <ol class="breadcrumb hide-phone p-0 m-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $data[0]['title']; ?></a></li>
                <li class="breadcrumb-item active">Tambahkan Pasien Baru</li>
              </ol>
            </div>
            <h4 class="page-title">Tambahkan Pasien Baru</h4>
          </div>
        </div>
      </div>
      <!-- end page title end breadcrumb -->
    </div> <!-- End Container -->
  </div><!-- End Wrapper -->
  <!-- ==================
     PAGE CONTENT START
    ================== -->
  <div class="page-content-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="m-b-20">
            <a href="<?php echo base_url('user/patients'); ?>"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-arrow-left"></i>&nbsp; Kembali Ke Daftar Pasien</button></a>
          </div>
        </div>
      </div><!-- Ends Row -->
      <div class="row">
        <div class="col-12">
          <div class="card m-b-20">
            <div class="card-block">
              <blockquote class="bg-info text-white">Informasi Umum</blockquote>
              <form name="addpatient" id="addpatient" method="post" action="<?php echo base_url('user_operation/addpatient'); ?>">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama Pasien</label>
                      <input type="text" class="form-control" name="name" required="" pattern="[A-Za-z. ]{1,}" placeholder="<?php echo $name; ?>" title="<?php echo $name; ?>">
                      <?php if(isset($error['name'])){?> <span class="text-danger"><?php echo $error['name']; ?></span> <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Usia Pasien</label>
                      <input type="text" class="form-control" name="age" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $age; ?>" title="<?php echo $age; ?>">
                      <?php if(isset($error['age'])){?> <span class="text-danger"><?php echo $error['age']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <select class="form-control" name="gender" required="">
                        <?php foreach ($gender as $key => $val) { ?>
                        <option value="<?php echo $key ?>" <?php echo ($key == 0) ? 'selected="selected" disabled="disabled"' : '' ?>><?php echo $val; ?></option>
                      <?php } ?>
                      </select>
                      <?php if(isset($error['gender'])){?> <span class="text-danger"><?php echo $error['gender']; ?></span> <?php } ?>
                    </div>
                  </div>
              <?php if (TYPE == 'vetbiz') { ?>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Jenis Hewan</label>
                      <input type="text" class="form-control" name="type" required="" pattern="[A-Za-z. ]{1,}" placeholder="<?php echo $type; ?>" title="<?php echo $type; ?>">
                      <?php if(isset($error['type'])){?> <span class="text-danger"><?php echo $error['type']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama Pemilik</label>
                      <input type="text" class="form-control" name="owner" required="" pattern="[A-Za-z. ]{1,}" placeholder="<?php echo $owner; ?>" title="<?php echo $owner; ?>">
                      <?php if(isset($error['owner'])){?> <span class="text-danger"><?php echo $error['owner']; ?></span> <?php } ?>
                    </div>
                  </div>
              <?php } ?>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nomor Telepon</label>
                      <input type="text" class="form-control" name="phone" required="" pattern="[\+0-9\-]{5,20}" placeholder="<?php echo $phone; ?>" title="<?php echo $phone; ?>">
                      <?php if(isset($error['phone'])){?> <span class="text-danger"><?php echo $error['phone']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Alamat Lengkap</label>
                      <textarea name="add" rows="3" class="form-control" placeholder="Masukkan Alamat Lengkap Pasien" required=""></textarea>
                      <?php if(isset($error['add'])){?> <span class="text-danger"><?php echo $error['add']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <blockquote class="bg-info text-white mt-5">Informasi Medis</blockquote>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Nomor Rekam Medis' : 'Tinggi'; ?></label>
                      <input type="text" class="form-control" name="height" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $height; ?>" title="<?php echo $height; ?>">
                      <?php if(isset($error['height'])){?> <span class="number-danger"><?php echo $error['height']; ?></span> <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Berat Badan</label>
                      <input type="text" class="form-control" name="weight" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $weight; ?>" title="<?php echo $weight; ?>">
                      <?php if(isset($error['weight'])){?> <span class="number-danger"><?php echo $error['weight']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Golongan Darah</label>
                      <select class="form-control" name="b_group" title="Pilih Hasil Pemeriksaan Golongan Darah" required="">
                      <?php foreach ($blood as $key => $val) { ?>
                        <option value="<?php echo $val ?>" <?php echo ($key == 0) ? 'selected="selected" disabled="disabled"' : '' ?>><?php echo $val; ?></option>
                      <?php } ?>
                      </select>
                      <?php if(isset($error['b_group'])){?> <span class="text-danger"><?php echo $error['b_group']; ?></span> <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'ID Microchip' : 'Tekanan Darah'; ?></label>
                      <input type="text" class="form-control" name="b_pressure" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $b_pressure; ?>" title="<?php echo $b_pressure; ?>">
                      <?php if(isset($error['b_pressure'])){?> <span class="text-danger"><?php echo $error['b_pressure']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Suhu Tubuh' : 'Denyut Nadi'; ?></label>
                      <input type="text" class="form-control" name="pulse" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $pulse; ?>" title="<?php echo $pulse; ?>">
                      <?php if(isset($error['pulse'])){?> <span class="number-danger"><?php echo $error['pulse']; ?></span> <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Frekuensi Pernafasan' : 'Respirasi'; ?></label>
                      <input type="text" class="form-control" name="respiration" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $respiration; ?>" title="<?php echo $respiration; ?>">
                      <?php if(isset($error['respiration'])){?> <span class="text-danger"><?php echo $error['respiration']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Anamnesa' : 'Alergi & Penyakit Saat Ini'; ?></label>
                      <input type="text" class="form-control" name="allergy" required="" placeholder="<?php echo $allergy; ?>" title="<?php echo $allergy; ?>">
                      <?php if(isset($error['allergy'])){?> <span class="text-danger"><?php echo $error['allergy']; ?></span> <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Tingkah Laku' : 'Diet'; ?></label>
                      <input type="text" class="form-control" name="diet" required="" placeholder="<?php echo $diet; ?>" title="<?php echo $diet; ?>">
                      <?php if(isset($error['diet'])){?> <span class="text-danger"><?php echo $error['diet']; ?></span> <?php } ?>
                    </div>
                  </div>
                </div>
                <div id="medinfo" rel="0"></div>
                <div class="row">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Tambahkan Pasien Baru</button>
                  </div>
                  <div class="col-md-6">
                    <button type="button" onclick="addform('medinfo');" class="btn btn-info waves-effect waves-light pull-right">Tambahkan Info</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!-- Ends Row -->
    </div><!-- Ends container -->
  </div><!-- Ends page-content-wrapper -->
<?php include_once('includes/footer_start.php'); ?>

<script type="text/javascript">
// $(function() {});

/*function addform(id){
  var rel = parseInt($('#' + id).attr('rel'));
  var even = '<div class="col-md-6 medinfo' + (rel + 1) + '"><div class="form-group"><div class="row"><div class="col-md-4"><input type="text" class="form-control" name="other[' + rel + '][label]" placeholder="Masukkan Label" required=""></div><div class="col-md-6"><input type="text" class="form-control" name="other[' + rel + '][value]" placeholder="Masukkan Informasi" required=""></div><div class="col-md-2"><button type="button" onclick="remform(' + (rel + 1) + ');" class="btn btn-danger waves-effect waves-light pull-right"><i class="fa fa-close"></i></button></div></div></div></div>';
  var html = (rel % 2 == 0) ? '<div id="medinfo' + (rel + 1) + '" class="row medinfo' + (rel + 1) + '">' + even + '</div>' : even ;
  $('#' + id).attr('rel', rel + 1);
  id += (rel % 2 == 0) ? '' : rel ;
  $('#' + id).append(html);
}*/

function addform(id){
  var rel = parseInt($('#' + id).attr('rel'));
  var html = '<div class="row medinfo' + (rel + 1) + '"><div class="col-md-2"></div><div class="col-md-8"><div class="form-group"><div class="row"><div class="col-md-4"><input type="text" class="form-control" name="other[' + rel + '][label]" placeholder="Masukkan Label" required=""></div><div class="col-md-6"><input type="text" class="form-control" name="other[' + rel + '][value]" placeholder="Masukkan Informasi" required=""></div><div class="col-md-2"><button type="button" onclick="remform(' + "'medinfo" + (rel + 1) + "'" + ');" class="btn btn-danger waves-effect waves-light pull-right"><i class="fa fa-close"></i></button></div></div></div></div><div class="col-md-2"></div></div>';
  $('#' + id).attr('rel', rel + 1);
  $('#' + id).append(html);
}

function remform(id){
  $('.' + id).remove();
}

</script>

<?php include_once('includes/footer_end.php'); ?>