<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  $data = $this->user_mo->img();
?>

<!DOCTYPE html>
<html>
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?php echo $data[0]['title']." - ".$title; ?></title>
    <meta content="<?php echo ucfirst(TYPE); ?> Resep - Sistem Resep Online" name="description" />
    <meta content="Landinghub(themesbrand)" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

    <!-- Basic Css files -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

  </head>
  <body class="fixed-left">

    <!-- Loader -->
    <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">
      <div class="card">
        <div class="card-block">
          <h3 class="text-center m-0">
            <a href="https://resep.promedis.id" class="logo logo-admin"><img src="<?php echo base_url(); ?>assets/images/<?php echo $data[0]['logo']; ?>" height="48" alt="logo"></a>
          </h3>

          <div class="p-3">
            <h4 class="text-muted font-18 m-b-5 text-center">Perbarui Kata Sandi</h4>
            <p class="text-muted text-center"><label class="status">Masukkan Email Dokter dan Petunjuk Akan Diemail ke Dokter!</label></p>
            <p class="text-muted text-center">Terima Kasih Atas Kerjasamanya</p>
            <form class="form-horizontal m-t-30">
              <div class="form-group">
                <label for="useremail">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Masukkan Email Yang Telah Didaftarkan" required="">
              </div>

              <div class="form-group row m-t-20">
                <div class="col-12 text-right">
                  <button class="btn btn-primary w-md waves-effect waves-light enabled" type="submit" id="reset">Perbarui</button>
                </div>
              </div>
            </form>
          </div>
      <div class="m-t-40 text-center">
        <p class="text-black">Sudah Ingat Kata Sandi Dokter? <a href="<?php echo base_url(); ?>" class="font-500 font-14 text-black font-secondary"> Masuk Disini </a> </p>
        <p class="text-black">© <?php echo date('Y').' '.$data[0]['title']; ?>. Dibuat Dengan <i class="mdi mdi-lock"></i> PS Global Media</p>
      </div>
    </div>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

    <!-- jQuery  -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>

    <script>
      $('document').ready(function(){
        $('#reset').click(function(e){
          e.preventDefault();
          var email = $('#email').val();
          var base_url = $('#base_url').val();
          if(email) {
            $.ajax({
              type:'POST',
              url:base_url + 'user_operation/recoverpassword',
              dataType:'json',
              data:{email:email},
              success: function(data){
                if(data.status == 1) {
                  $(".status").css('color','green');
                  $('.status').html('Kata Sandi Dokter akan dikirimkan ke email yang terdaftar.');
                } else {
                  $(".status").css('color','red');
                  $('.status').html('Silahkan Masukkan Email Yang Terdaftar.');
                }
              },
              error:function() {
                alert('Oops! Email yang dimasukkan tidak terdaftar!!!');
              }
            });
          } else {
            alert('Silahkan Masukkan Email Dokter Yang Terdaftar.!!');
            $('#email').focus();
          }
        });
      });
    </script>
    
  </body>
</html>