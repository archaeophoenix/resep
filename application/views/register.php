<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*$data = $this->user_mo->get_user();
if(!$data) {
  redirect(base_url('user/setup'));
} else {*/
if(isset($_SESSION['userinfo'])) {
  redirect(base_url('user/dashboard'));
} else {
?>

<!DOCTYPE html>
<html>
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Sistem Resep Online</title>
    <meta content="<?php echo ucfirst(TYPE); ?> Resep - Sistem Resep Online" name="deskripsi" />
    <meta content="Landinghub(themesbrand)" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/ic_promedis1.png">

    <!-- Basic Css files -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

  </head>

  <body class="fixed-left">
    <!-- Loader -->
    <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">
      <div class="card">
        <div class="card-block">
          <h3 class="text-center m-0">
            <a href="https://promedis.id" class="logo logo-admin"><img src="<?php echo base_url(); ?>assets/images/logo_promedis_new600x143.png" height="48" alt="logo"></a>
          </h3>

          <div class="p-3">
            <h4 class="text-muted font-18 m-b-5 text-center">SELAMAT DATANG !</h4>
            <p class="text-muted text-center">Silahkan Masuk ke <?php echo ucfirst(TYPE); ?> Resep.</p>
            <form class="form-horizontal m-t-30" method="post" action="<?php echo base_url('user_operation/register'); ?>">
              <div class="username form-group">
                <label class="form-control-label" for="username">Nama Dokter</label>
                <input type="text" class="form-control" id="username" name="user_name" oninput="usernameCheck(this.value);" placeholder="Masukkan Nama Pengguna Dokter" autofocus="autofocus">
                <div id="username2" class="form-control-feedback" style="display: none;">Sorry, that username's taken. Try another?</div>
              </div>

              <div class="email form-group">
                <label class="form-control-label" for="email">Email Dokter</label>
                <input type="email" class="form-control" id="email" name="email" oninput="usernameCheck(this.value);" placeholder="Masukkan Email Pengguna Dokter" autofocus="autofocus">
                <div id="email2" class="form-control-feedback" style="display: none;">Sorry, that email's taken. Try another?</div>
              </div>

              <div class="form-group">
                <label for="userpassword">Kata Sandi</label>
                <input type="password" class="form-control" id="password" name="password" oninput="usernameCheck(username.value);" placeholder="Masukkan Kata Sandi">
              </div>

              <div class="form-group">
                <label for="userpassword">Konfirmasi Kata Sandi</label>
                <input type="password" class="form-control" id="password2" name="password2" oninput="usernameCheck(username.value);" placeholder="Konfirmasi Kata Sandi">
              </div>

              <div class="form-group row m-t-20">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-6 text-right">
                  <button class="btn btn-primary w-md waves-effect waves-light" disabled="disabled" id="submit" type="submit">Register</button>
                </div>
              </div>
              <div class="form-group m-t-10 mb-0 row">
                <div class="col-12 m-t-20">
                  <a href="<?php echo base_url(); ?>" class="text-muted"><i class="mdi mdi-book"></i> Dokter Silahkan Login Disini</a>
              
            </form>
          </div>
        </div>
      </div>

      <div class="m-t-40 text-center">
        <p class="text-grey">© <?php echo date('Y'); ?> . Dibuat Dengan <i class="mdi mdi-lock"></i>PS Global Media</p>
      </div>
    </div>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

    <!-- jQuery  -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>

    <script type="text/javascript">
      var base_url = $('#base_url').val();

      async function usernameCheck(){
        var email = $('#email').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var password2 = $('#password2').val();

        var liam = {email: email};
        var atad = {username: username};

        var mail = await $.post(base_url + 'user_operation/email', liam);
        var data = await $.post(base_url + 'user_operation/username', atad);

        var liame = (mail.code == 204) ? true : false ;
        var enamresu = (data.code == 204) ? true : false ;

        var mailgro = (liame) ? '' : 'has-danger';
        var mailcon = (liame) ? '' : 'form-control-danger';

        var resugro = (enamresu) ? '' : 'has-danger';
        var resucon = (enamresu) ? '' : 'form-control-danger';

        if (enamresu) {
          $('#username2').hide();
        } else {
          $('#username2').show();
        }

        if (liame) {
          $('#email2').hide();
        } else {
          $('#email2').show();
        }

        if (liame == true && enamresu == true) {
          if ((email !== '') && (username !== '') && (password !== '') && (password2 !== '') && (password === password2)) {
            $('#submit').removeAttr('disabled');
          } else {
            $('#submit').attr('disabled', 'disabled');
          }
        }

        $('.email').attr('class', 'email form-group ' + mailgro);
        $('#email').attr('class', 'form-control ' + mailcon);

        $('.username').attr('class', 'username form-group ' + resugro);
        $('#username').attr('class', 'form-control ' + resucon);
      }

      $(function () {});
    </script>

  </body>
</html>
<?php } ?>
<?php //} ?>