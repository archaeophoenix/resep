<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_operation extends CI_Controller {

  function __construct(){
    parent::__construct();
    /*if (empty($this->session->userdata('userdata'))) {
      redirect(base_url('user/'));
    }*/
  }

  public function setup() {
    $this->form_validation->set_rules('dname', 'Doctor_name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^[0-9\+-]+$/]');
    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('cpassword', 'Confirm_Password', 'required');
    $this->form_validation->set_rules('title', 'System_title', 'required');
    if ($this->form_validation->run() == FALSE) {
      $error['dname'] = form_error('dname');
      $error['email'] = form_error('email');
      $error['phone'] = form_error('phone');
      $error['username'] = form_error('username');
      $error['password'] = form_error('password');
      $error['cpassword'] = form_error('cpassword');
      $error['title'] = form_error('title');
      $this->session->set_flashdata('error', $error);
      redirect(base_url().'user/setup');
    } else {
      $config['upload_path']   = 'assets/images'; 
      $config['allowed_types'] = 'jpg|png|ico'; 
      $config['max_size']   = 100; 
      $config['max_width']   = 1024; 
      $config['max_height'] = 1280;  

      $this->load->library('upload', $config); 
      
      if ( ! $this->upload->do_upload('logo')){ // file upload in folder
        $error = array('error' => $this->upload->display_errors()); 
        $this->session->set_flashdata('image', $error);
        redirect(base_url().'user/setup'); // user redirect    
      }
      else { 
        $data = $this->input->post(); /// USer NAme

        $dataup = array('upload_data' => $this->upload->data()); /// file name 
        $this->upload->do_upload('favicon');
        $dataup1 = array('upload_data' => $this->upload->data()); /// file name 
        $dataedit = array('user_name'=>$data['username'],'doctor_name'=>$data['dname'],'email'=>$data['email'],'mobile'=>$data['phone'],'password'=>base64_encode($data['password']),'logo'=>$dataup['upload_data']['file_name'],'favicon'=>$dataup1['upload_data']['file_name'],'title'=>$data['title']); // array arrange based on table
        $res = $this->user_mo->setup($dataedit);
        if($res) {
          redirect(base_url());
        } else {
          redirect(base_url().'user/setup');
        }
      }
    }
  }

  function profile (){
    return (TYPE == 'promedis') ? 'Promedis' : 'Promedis';
  }

  function confirm() {
    $profile = $this->profile();
    $data = $this->input->post();
    $status = ['Tidak Aktif', 'Aktif'];

    $send = $this->user_mo->read('users', 'WHERE status = 2 OR user_id = ' . $data['user_id'], 'email');
    $update = $this->user_mo->updateusers($data['user_id'], $data);

    foreach ($send as $key => $val) {
      $data['subject'] = $profile . ' Resep - Informasi Akun';
      $data['email'] = $val['email'];
      $data['title'] = $profile . ' Resep - Informasi Akun';
      $data['text'] = '<p>Kami telah mengkonfirmasi status pengguna ' . $profile . ' Resep.</p>
      <p>Nama Pengguna yang digunakan adalah : ' . $data['user_name'] . '</p>
      <p>Status : ' . $status[$data['status']] . '</p>
      <p>Salam</p>
      <p>' . $profile . ' Resep</p>';
      $this->mail($data);
    }

    if($update) {
      redirect(base_url('user/users/' . $data['user_id']));
    } else {
      echo 'error';
    }
  }

  function users() {
    $user = $this->session->userdata('userid');
    $page = (isset($_GET['page'])) ? $this->input->get('page') : 1 ;
    $name = (isset($_GET['name'])) ? $this->input->get('name') : '';
    $name = (isset($_GET['name'])) ? ' AND (user_name LIKE "%' . $name . '%" OR doctor_name LIKE "%' . $name . '%" OR email LIKE "%' . $name . '%") ' : '';

    $field = 'user_id, user_name, doctor_name, email, mobile, logo, favicon, title, office, sip, specialist, status';
    $count = $this->user_mo->one('users', 'WHERE user_id NOT IN (' . $user . ')' . $name, 'COUNT(user_id) AS row');

    $data['rows'] = ceil($count['row'] / 10);

    $start  = ($page < 3)? 1 : $page - 2;
    $end    = 4 + $start;
    $end    = ($data['rows'] < $end) ? $data['rows'] : $end;
    $diff   = $start - $end + 4;
    $start -= ($start - $diff > 0) ? $diff : 0;

    $data['first'] = ($start > 1) ? '&laquo;' : '' ;
    $data['last']  = ($end < $data['rows']) ? '&raquo;' : '' ;
    $data['st1']   = $start;
    $data['end']   = $end;
    $data['page']  = $page;
    $data['count'] = $count['row'];

    $page  = ($page - 1) * 10;

    $data['user']  = $this->user_mo->read('users', 'WHERE user_id NOT IN (' . $user . ')' . $name . 'ORDER BY status ASC, user_id DESC LIMIT ' . $page . ', 10', $field);

    header('Content-type: application/json');
    echo json_encode($data);
  }

  function username() {
    $username = $this->input->post('username');
    $data = $this->user_mo->one('users', 'WHERE user_name = "' . $username . '" ORDER BY user_id DESC LIMIT 1', 'user_id');
    $return = (empty($data['user_id'])) ? ['code' => 204, 'text' => 'No Content'] : ['code' => 200, 'text' => 'OK'] ;
    header('Content-type: application/json');
    echo json_encode($return);
  }

  function email() {
    $email = $this->input->post('email');
    $data = $this->user_mo->one('users', 'WHERE email = "' . $email . '" ORDER BY user_id DESC LIMIT 1', 'user_id');
    $return = (empty($data['user_id'])) ? ['code' => 204, 'text' => 'No Content'] : ['code' => 200, 'text' => 'OK'] ;
    header('Content-type: application/json');
    echo json_encode($return);
  }

  public function login() {
    $user_name = $this->input->post('username');
    $pwd = base64_encode($this->input->post('password'));
    $data = $this->user_mo->login_data($user_name,$pwd);

    if($data) {
			$this->session->set_userdata('userinfo',$data['user_name']);
      $this->session->set_userdata('userdata',$data['user_name']);
      $this->session->set_userdata('userid',$data['user_id']);
      $this->session->set_userdata('userstatus',$data['status']);
      redirect(base_url('user/dashboard'));
    } else {
      $this->session->set_flashdata('msg','Please Check Your Credentials!!');
      redirect(base_url());
    }
    
  }
  
  public function register() {
    $post = $this->input->post();
    $this->form_validation->set_rules('user_name', 'Username', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('password2', 'Password', 'required');
    if ($this->form_validation->run() == TRUE && $post['password'] == $post['password2']) {
      if (empty( $this->session->userdata('userinfo') ) ) {
        unset($post['password2']);
        $post['password'] = base64_encode($post['password']);
        $atad = $this->user_mo->setup($post);
        $this->session->set_userdata('userinfo',$post['user_name']);
        $this->session->set_userdata('userdata',$post['user_name']);
        $this->session->set_userdata('userid',$atad);
        
        $admin = $this->user_mo->read('users', 'WHERE status = 2', 'email');

        foreach ($admin as $key => $val) {
          $profile = $this->profile();
          $data['subject'] = $profile . ' Resep - Informasi Akun Baru';
          $data['email'] = $val['email'];
          $data['title'] = $profile . ' Resep - Informasi Akun Baru';
          $data['text'] = '<p>Kami telah menerima permintaan untuk pendaftaran pengguna baru ' . $profile . ' Resep. Semoga ' . $profile . ' Resep bermanfaat untuk praktek sehar-hari.</p>
          <p>Jika merasa tidak membuat akun dengan email ini maka hubungi kami segera atau silahkan hapus saja data akunnya.</p>
          <p>Nama Pengguna yang digunakan adalah : ' . $post['user_name'] . '</p>
          <p>Kami ucapkan terima kasih atas kerjasamanya</p>
          <p>Salam Sukses</p>
          <p>' . $profile . ' Resep</p>';
          $this->mail($data);
        }
      }
      redirect(base_url() . 'user/profile');
    }
  }

  public function logout() {
    $this->session->sess_destroy();
		redirect(base_url());
  }

  public function addpatient() {
    $this->form_validation->set_rules('name', 'Patient name', 'required');
    $this->form_validation->set_rules('age', 'Age', 'required|numeric');
    $this->form_validation->set_rules('gender', 'Gender', 'required');
    $this->form_validation->set_rules('phone', 'Phone Number', 'required|regex_match[/^[0-9\+-]+$/]');
    $this->form_validation->set_rules('add', 'Address', 'required');
    $this->form_validation->set_rules('height', 'Height', 'required');
    $this->form_validation->set_rules('weight', 'Weight', 'required');
    $this->form_validation->set_rules('b_group', 'Blood Group', 'required');
    $this->form_validation->set_rules('b_pressure', 'Blood Pressure', 'required');
    $this->form_validation->set_rules('pulse', 'Pulse', 'required');
    $this->form_validation->set_rules('respiration', 'Respiration', 'required');
    $this->form_validation->set_rules('allergy', 'Allergy', 'required');
    $this->form_validation->set_rules('diet', 'Diet', 'required');

    if (TYPE == 'vetbiz') {
      $this->form_validation->set_rules('type', 'Type', 'required');
      $this->form_validation->set_rules('owner', 'Owner', 'required');
    }

    if ($this->form_validation->run() == FALSE) {
      $error['name'] = form_error('name');
      $error['age'] = form_error('age');
      $error['gender'] = form_error('gender');
      $error['phone'] = form_error('phone');
      $error['add'] = form_error('add');
      $error['height'] = form_error('height');
      $error['weight'] = form_error('weight');
      $error['b_group'] = form_error('b_group');
      $error['b_pressure'] = form_error('b_pressure');
      $error['pulse'] = form_error('pulse');
      $error['respiration'] = form_error('respiration');
      $error['allergy'] = form_error('allergy');
      $error['diet'] = form_error('diet');

      if (TYPE == 'vetbiz') {
        $error['type'] = form_error('type');
        $error['owner'] = form_error('owner');
      }

      $this->session->set_flashdata('error', $error);
      redirect(base_url().'user/addpatient');
    } else {
      $data['p_name'] = $this->input->post('name');
      $data['age'] = $this->input->post('age');
      $data['gender'] = $this->input->post('gender');
      $data['phone'] = $this->input->post('phone');
      $data['add'] = $this->input->post('add');
      $data['height'] = $this->input->post('height');
      $data['weight'] = $this->input->post('weight');
      $data['b_group'] = $this->input->post('b_group');
      $data['b_pressure'] = $this->input->post('b_pressure');
      $data['pulse'] = $this->input->post('pulse');
      $data['respiration'] = $this->input->post('respiration');
      $data['allergy'] = $this->input->post('allergy');
      $data['diet'] = $this->input->post('diet');
      $data['other'] = json_encode($this->input->post('other'));

      if (TYPE == 'vetbiz') {
        $data['type'] = $this->input->post('type');
        $data['owner'] = $this->input->post('owner');
      }

      if($this->user_mo->addpatient($data)) {
        redirect(base_url('user/patients'));
      } else {

      }
    }
  }

  public function editpatient() {
    $data['patient_id'] = $this->input->post('patient_id');
    $data['p_name'] = $this->input->post('name');
    $data['age'] = $this->input->post('age');
    $data['gender'] = $this->input->post('gender');
    $data['phone'] = $this->input->post('phone');
    $data['add'] = $this->input->post('add');
    $data['height'] = $this->input->post('height');
    $data['weight'] = $this->input->post('weight');
    $data['b_group'] = $this->input->post('b_group');
    $data['b_pressure'] = $this->input->post('b_pressure');
    $data['pulse'] = $this->input->post('pulse');
    $data['respiration'] = $this->input->post('respiration');
    $data['allergy'] = $this->input->post('allergy');
    $data['diet'] = $this->input->post('diet');
    $data['other'] = json_encode($this->input->post('other'));

    if (TYPE == 'vetbiz') {
      $data['type'] = $this->input->post('type');
      $data['owner'] = $this->input->post('owner');
    }

    if($this->user_mo->editpatient($data)) {
      redirect(base_url('user/patient_profile/'.$data['patient_id']));
    } else {

    }
  }

  public function deletepatient() {
    $id = $this->uri->segment(3);
    if($this->user_mo->deletepatient($id)) {
      redirect(base_url('user/patients'));
    }
  }

  public function chkappointment() {
    $data['patient_id'] = $this->input->post('p_id');
    $data['date'] = $this->input->post('date');
    $data['time'] = $this->input->post('time');

    $res = $this->user_mo->chkappointment($data); 
    header('Content-type: application/json');
    if($res ==1) {
      echo json_encode(array('status'=>1));
    } elseif($res == 2) {
      echo json_encode(array('status'=>2));
    } else {
      echo json_encode(array('status'=>0));
    }
  }

  public function addprescription() {
    $this->form_validation->set_rules('patient_id', 'Patient', 'required');
    $this->form_validation->set_rules('symptoms', 'Symptoms', 'required');
    $this->form_validation->set_rules('diagnosis', 'Diagnosis', 'required');

    if ($this->input->post('type') == '#medicine') {
      $this->form_validation->set_rules('medicine_name[]', 'Medicine_name', 'required');
      $this->form_validation->set_rules('medicine_note[]', 'Medicine_note', 'required');
    } else {
      $this->form_validation->set_rules('test_name[]', 'Test_name', 'required');
      $this->form_validation->set_rules('test_note[]', 'Test_note', 'required');
    }

    if ($this->form_validation->run() == FALSE) {
      $error['patient_id'] = form_error('patient_id');
      $error['symptoms'] = form_error('symptoms');
      $error['diagnosis'] = form_error('diagnosis');

      if ($this->input->post('type') == '#medicine') {
        $error['medicine_name'] = form_error('medicine_name[]');
        $error['medicine_note'] = form_error('medicine_note[]');
      } else {
        $error['test_name'] = form_error('test_name[]');
        $error['test_note'] = form_error('test_note[]');
      }

      $this->session->set_flashdata('error', $error);
      redirect(base_url().'user/addprescription');

    } else {
      $data['patient_id'] = $this->input->post('patient_id');
      $data['symptoms'] = $this->input->post('symptoms');
      $data['diagnosis'] = $this->input->post('diagnosis');
      $data['date'] = date('Y-m-d');

      if ($this->input->post('type') == '#medicine') {
        $data['medicine'] = json_encode($this->input->post('medicine_name[]'));
        $data['m_note'] = json_encode($this->input->post('medicine_note[]'));
        $data['m_note2'] = json_encode($this->input->post('medicine_note2[]'));
      } else {
        $data['test'] = json_encode($this->input->post('test_name[]'));
        $data['t_note'] = json_encode($this->input->post('test_note[]'));
        $data['t_note2'] = json_encode($this->input->post('test_note2[]'));
      }
      
      if($this->user_mo->addprescription($data)) {
        $res = $this->db->query('select max(prescription_id) as id from prescription')->result_array();
        redirect(base_url('user/print_prescription/').$res[0]['id']);
      } else {
        
      }
    }
  }

  public function deleteprescription() {
    $id = $this->uri->segment(3);
    if($this->user_mo->deleteprescription($id)) {
      redirect(base_url('user/prescription'));
    }
  }

  public function createinvoice() {
    $this->form_validation->set_rules('patient_id', 'Patient', 'required');
    $this->form_validation->set_rules('payment_mode', 'Payment_Mode', 'required');
    $this->form_validation->set_rules('payment_status', 'Payment_Status', 'required');
    $this->form_validation->set_rules('invoice_title[]', 'Invoice_Title', 'required|alpha_numeric_spaces');
    $this->form_validation->set_rules('invoice_amount[]', 'Invoice_amount', 'required|numeric');
    if ($this->form_validation->run() == FALSE) {
      $error['patient_id'] = form_error('patient_id');
      $error['payment_mode'] = form_error('payment_mode');
      $error['payment_status'] = form_error('payment_status');
      $error['invoice_title'] = form_error('invoice_title[]');
      $error['invoice_amount'] = form_error('invoice_amount[]');
      $this->session->set_flashdata('error', $error);
      redirect(base_url().'user/createinvoice');
    } else {
      $data['patient_id'] = $this->input->post('patient_id');
      $data['payment_mode'] = $this->input->post('payment_mode');
      $data['payment_status'] = $this->input->post('payment_status');
      $data['invoice_title'] = json_encode($this->input->post('invoice_title[]'));
      $data['invoice_amount'] = json_encode($this->input->post('invoice_amount[]'));
      $data['invoice_date'] = date('Y-m-d');
      
      if($this->user_mo->createinvoice($data)) {
        $res = $this->db->query('select max(invoice_id) as id from invoice')->result_array();
        redirect(base_url('user/print_invoice/').$res[0]['id']);
      } else {
        
      }
    }
  }

  public function deleteinvoice() {
    $id = $this->uri->segment(3);
    if($this->user_mo->deleteinvoice($id)) {
      redirect(base_url('user/billing'));
    }
  }

  function updateoffice() {
    $data = $this->input->post();
    $user = $this->session->userdata('userid');
    $data['office'] = json_encode($data['office']);

    if($this->user_mo->updateusers($user, $data)) {
      redirect(base_url('user/profile'));
    }    
  }

  public function updateprofile() {
    $data['name'] = $this->input->post('name');
    $data['title'] = $this->input->post('title');
    $data['email'] = $this->input->post('email');
    $data['phone'] = $this->input->post('phone');
    $data['user_name'] = $this->session->userdata('userinfo');

    if($this->user_mo->updateprofile($data)) {
      redirect(base_url('user/profile'));
    }
  }

  public function updatepassword() {
    // $user = $this->session->userdata('userinfo');
    $data['cpass'] = base64_encode($this->input->post('cpass'));
    $data['password'] = base64_encode($this->input->post('npass'));
    $data['user_name'] = $this->session->userdata('userinfo');

    if($this->user_mo->updatepassword($data)) {
      header('Content-type: application/json');
      echo json_encode(array('status'=>1));   
    } else {
      header('Content-type: application/json');
      echo json_encode(array('status'=>2));   
    }
  }

  public function updatelogo($type = 'logo') {
    $config['upload_path']   = 'assets/images'; 
    $config['allowed_types'] = 'jpg|png|ico'; 
    $config['max_size']   = 100; 
    $config['max_width']   = 1024; 
    $config['max_height'] = 1280;  

    $this->load->library('upload', $config); 
    
    if ( ! $this->upload->do_upload('logo')){ // file upload in folder
      $error = array('error' => $this->upload->display_errors()); 
      $this->session->set_flashdata('success', $error);
      redirect(base_url().'user/profile'); // user redirect    
    } else {
      $dataup = array('upload_data' => $this->upload->data()); /// file name 
      $res = $this->user_mo->updatelogo($dataup['upload_data']['file_name'], $type);
      if($res) {
        redirect(base_url().'user/profile');
      } else {
        redirect(base_url().'user/profile');
      }
    }
  }

  public function appointmentlist() {
    $dt = $_GET['date'];
    $res = $this->user_mo->appointmentlist($dt);
    header('Content-type: application/json');
    if($res) {
      echo json_encode($res);
    } else {
      echo json_encode(array('status'=>1));
    }
  }

  function bulan(){
    return [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'];
  }

  public function appointmentchart() {
    $parent = array();
    $bulan = $this->bulan();
    $patient = $this->user_mo->patientchart();
    $appointment = $this->user_mo->appointmentchart();
    $prescription = $this->user_mo->prescriptionchart();

    $i = 0;
    $m = date('m') + 1;
    while ($m < 13) {
      // ['y'] ass month
      // $parent[$i]['y'] = date("F", mktime(0, 0, 0, $m, 10));
      $parent[$i]['num'] = $m;
      $parent[$i]['y'] = $bulan[$m];
      $m++;
      $i++;
    }

    $m = 1;
    while ($m <= date('m')) {
      // ['y'] ass month
      // $parent[$i]['y'] = date("F", mktime(0, 0, 0, $m, 10));
      $parent[$i]['num'] = $m;
      $parent[$i]['y'] = $bulan[$m];
      $m++;
      $i++;
    }

    foreach ($parent as &$arr1) {
      $offer_id = $arr1['num'];

      $pati = array_filter($patient, function($v) use ($offer_id){
        return $v['num_month'] == $offer_id;
      });
      $pati = array_pop($pati);
  
      $appo = array_filter($appointment, function($v) use ($offer_id){
        return $v['num_month'] == $offer_id;
      });
      $appo = array_pop($appo);

      $pres = array_filter($prescription, function($v) use ($offer_id){
        return $v['num_month'] == $offer_id;
      });
      $pres = array_pop($pres);

      //a as patient
      $arr1['a'] = (empty($pati['total'])) ? 0 : $pati['total'];
      //b as appointment
      $arr1['b'] = (empty($appo['total'])) ? 0 : $appo['total'];
      //c as presciption
      $arr1['c'] = (empty($pres['total'])) ? 0 : $pres['total'];

      unset($arr1['num']);
    }

    header('Content-type: application/json');
    if($parent) {
      echo json_encode($parent);
    } else {
      echo json_encode(array('status'=>1));
    }
  }

  public function invoicechart() {
    $res = $this->user_mo->invoicechart();

    header('Content-type: application/json');
    if($res) {
      echo json_encode($res);
    } else {
      echo json_encode(array('status'=>1));
    }
  }

  function cobamail(){
    $data['subject'] = ' Resep - Lupa Kata Sandi';
    $data['email'] = 'helo@promedis.id';
    $data['title'] = 'Yth Pengguna Resep';
    $data['text'] = '<p>Kami telah menerima permintaan lupa kata sandi dari akun, dibawah ini adalah kata sandi saat ini. Jika tidak pernah meminta lupa kata sandi maka segera masuk ke akun dan lakukan perubahan kata sandi.</p>
      <p>Kata Sandi saat ini adalah : </p><p>Kami ucapkan terima kasih atas kerjasamanya</p>
      <p>Salam Sukses</p>
      <p> Resep</p>';
      $this->mail($data);
  }

  function mail($mailer){
    extract($mailer);
    $htmlContent = '<h5>' . $title . '</h5>';
    $htmlContent .= $text;

    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'ams100.greengeeks.net';
    $config['smtp_user'] = (TYPE == 'promedis') ? 'resep@promedis.id' /*resep.promedis.id*/ : 'resep@promedis.id' /*resep.promedis.id*/ ;
    $config['smtp_pass'] = (TYPE == 'promedis') ? 'I3!+9T62_G~t' /*resep.promedis.id*/ : 'I3!+9T62_G~t' /*resep.promedis.id*/ ;
    $config['smtp_port'] = 587;
    $config['mailtype'] = 'html';
    //$config['smtp_crypto']=>'ssl',

    $this->email->initialize($config);
    $this->email->set_newline("\r\n");
    $this->email->from($config['smtp_user']);
    $this->email->to($email);
    $this->email->subject($subject);
    $this->email->message($htmlContent);
    if(!$this->email->send()) {
      show_error($this->email->print_debugger());
    } else {
      header('Content-type: application/json');
      echo json_encode(array('status'=>1));
    }
  }

  public function recoverpassword() {
    $email = $this->input->post('email');
    $user = $this->user_mo->getemail($email);

    if(count($user) == 1) {
      $profile = $this->profile();
      $data['subject'] = $profile . ' Resep - Lupa Kata Sandi';
      $data['email'] = $email;
      $data['title'] = 'Yth Pengguna ' . $profile . ' Resep';
      $data['text'] = '<p>Kami telah menerima permintaan lupa kata sandi dari akun, dibawah ini adalah kata sandi saat ini. Jika tidak pernah meminta lupa kata sandi maka segera masuk ke akun dan lakukan perubahan kata sandi.</p>
      <p>Kata Sandi saat ini adalah : ' . base64_decode($user[0]['password']) .
      '</p><p>Kami ucapkan terima kasih atas kerjasamanya</p>
      <p>Salam Sukses</p>
      <p>' . $profile . ' Resep</p>';
      $this->mail($data);
    } else {
      header('Content-type: application/json');
      echo json_encode(array('status'=>0));
    }
  }
}