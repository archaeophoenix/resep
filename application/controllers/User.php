<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index() {
		$this->load->library('migration');

		if ($this->migration->current() === FALSE) {
			show_error($this->migration->error_string());
		}
		if (!empty($this->session->userdata('userdata'))) {
			redirect(base_url('user/dashboard'));
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	function unconfirmed(){
		if (!empty($this->session->userdata('userdata'))) {
			if ($this->session->userdata('userstatus') != 0) {
				redirect(base_url('user/dashboard'));
			}
			$data['title'] = "Unconfirmed";
			$this->load->view('unconfirmed',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	function register() {
		if (!empty($this->session->userdata('userdata'))) {
			redirect(base_url('user/dashboard'));
		} else {
			$this->session->sess_destroy();
			$this->load->view('register');
		}
	}

	public function setup() {
		$data['title'] = "Instalasi Halaman";
		$this->load->view('setup',$data);
	}

	public function dashboard() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Ringkasan Eksekutif";
			$data['total'] = $this->user_mo->total_count();
			$data['info'] = $this->user_mo->patient();
			$this->load->view('dashboard',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function appointment() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Daftar Appointment";
			$data['info'] = $this->user_mo->patient_info();
			$data['ap_list'] = $this->user_mo->appointment_list();
			$this->load->view('appointment',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function prescription() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Daftar Resep";
			$data['info'] = $this->user_mo->getprescription();
			$this->load->view('prescription',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function billing() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Daftar Tagihan";
			$data['stapay'] = $this->stapay();
			$data['info'] = $this->user_mo->getinvoice();
			$this->load->view('billing',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function patients() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Daftar Pasien";
			$data['info'] = $this->user_mo->patient_info();
			$this->load->view('patient',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	function users($id = null) {
		if (!empty($this->session->userdata('userdata'))) {
			if ($this->session->userdata('userstatus') != 2) {
				redirect(base_url('user/dashboard'));
			}

			if (!empty($id)) {
		    $field = 'user_id, user_name, doctor_name, email, mobile, logo, favicon, title, office, sip, specialist, status';
				$data['data'] = $this->user_mo->get_user();
				$data['info'] = $this->user_mo->one('users', 'WHERE user_id = ' .$id, $field);
				$data['appo'] = $this->user_mo->one('appointment', 'WHERE user_id = ' .$id, 'COUNT(appointment_id) AS total');
				$data['pati'] = $this->user_mo->one('patient', 'WHERE user_id = ' .$id, 'COUNT(patient_id) AS total');
				$data['pres'] = $this->user_mo->one('prescription', 'WHERE user_id = ' .$id, 'COUNT(prescription_id) AS total');
				$data['invo'] = 0;
				$data['stat'] = ['Tidak Aktif', 'Aktif'];

				$invo = $this->user_mo->read('invoice', 'WHERE user_id = ' . $id, 'invoice_amount');
				foreach ($invo as $key => $val) {
					$data['invo'] += array_sum(json_decode($val['invoice_amount'], true));
				}
			}

			$file = (empty($id)) ? 'users' : 'detail';
			$data['title'] = (empty($id)) ? 'Users List' : 'Users Detail';

			$this->load->view($file,$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function lockscreen() {
		$data['user'] = $this->user_mo->getuser();
		$this->load->view('lockscreen',$data);
	}

	public function recoverpassword() {
		$data['title'] = "Perbarui Kata Sandi";
		$this->load->view('recoverpassword',$data);
	}

	public function profile() {
		if (!empty($this->session->userdata('userdata'))) {
			$data['title'] = "Profil Dokter";
			$data['info'] = $this->user_mo->user_info();
			$this->load->view('profile',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	function blood(){
		$data['promedis'] = ['-- Pilih Golongan Darah --', 'A+', 'A-', 'B+', 'O+', 'O-', 'AB+', 'AB-'];
		return $data[TYPE];
	}

	function gender(){
		$data['promedis'] = ['--Pilih Jenis Kelamin--', 'Laki-Laki', 'Perempuan'];
		return $data[TYPE];
	}

	function stapay(){
		return ['Cash' => 'Tunai', 'Cheque' => 'Non Tunai', 'Paid' => 'Lunas', 'Unpaid' => 'Belum Lunas'];
	}


	public function addpatient() {
		// echo "<pre>";print_r('$data');die();
		// echo '<form method="post" action=""><input type="text" pattern="[0-9]+([\.,][0-9]+)?" name="name"><button type="submit">submit</button></form>';die();
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Tambahkan Pasien Baru";
			$data['blood'] = $this->blood();
			$data['gender'] = $this->gender();
			$this->load->view('addpatient',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function patient_profile() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Profil Pasien";
			$data['gender'] = $this->gender();
			$data['stapay'] = $this->stapay();
			$this->load->view('patientprofile',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function editpatient() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Ubah Profil Pasien";
			$data['blood'] = $this->blood();
			$data['gender'] = $this->gender();
			$this->load->view('editpatient',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function addprescription() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Tambahakan Resep";
			$data['info'] = $this->user_mo->patient_info();
			$this->load->view('addprescription',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function print_prescription() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Cetak Resep";
			$this->load->view('printprescription',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function createinvoice() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Buat Faktur Baru";
			$data['info'] = $this->user_mo->patient_info();
			$this->load->view('createinvoice',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	public function print_invoice() {
		if (!empty($this->session->userdata('userdata'))) {
			$this->userstatus();
			$data['title'] = "Cetak Faktur";
			$this->load->view('printinvoice',$data);
		} else {
			redirect(base_url('user/'));
		}
	}

	function userstatus(){
	    if (!empty($this->session->userdata('userdata')) && !isset($_SESSION['userinfo'])) {
			redirect(base_url('user/lockscreen'));
		}
		if ($this->session->userdata('userstatus') == 0) {
			redirect(base_url('user/unconfirmed'));
		}
	}
}